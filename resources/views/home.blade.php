@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if ($message = Session::get('error'))
                        <div class="alert alert-danger" role="alert">
                            {{ $message }}
                        </div>
                    @endif

                    @if ($errors->any())
                        <div class="alert alert-danger" role="alert">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @if ($message = Session::get('success'))
                        <div class="alert alert-success" role="alert">
                            {{ $message }}
                        </div>
                    @endif

                    <form action="{{ route('siswa_verif', Auth::user()->id) }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="nis">NIS</label>
                                <input id="nis" class="form-control" value="{{ Auth::user()->siswa->nis }}" readonly type="text" name="nis" required>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="nama">Nama</label>
                                <input id="nama" class="form-control" value="{{ Auth::user()->siswa->nama }}" readonly type="text" name="nama" required>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="email">Email</label>
                                <input id="email" class="form-control" value="{{ Auth::user()->siswa->email }}" readonly type="text" name="email" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="jk">Jenis Kelamin</label>
                                <select class="form-control" readonly name="jk" id="jk" required>
                                    <option value="{{ Auth::user()->siswa->jk }}" selected>{{ Auth::user()->siswa->jk }}</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="temp_lahir">Tempat Lahir</label>
                                <input id="temp_lahir" class="form-control" readonly value="{{ Auth::user()->siswa->temp_lahir }}" type="text" name="temp_lahir" required>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="tgl_lahir">Tanggal Lahir</label>
                                <input id="tgl_lahir" class="form-control" readonly type="date" value="{{ Auth::user()->siswa->tgl_lahir }}" name="tgl_lahir" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="alamat">Alamat</label>
                                <textarea class="form-control" name="alamat" readonly id="alamat" cols="30" rows="10" required>{{ Auth::user()->siswa->alamat }}</textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="asal_sekolah">Asal Sekolah</label>
                                <input id="asal_sekolah" class="form-control" readonly type="text" value="{{ Auth::user()->siswa->asal_sekolah }}" name="asal_sekolah" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="kelas">Kelas</label>
                                <input id="kelas" class="form-control" type="text" readonly value="{{ Auth::user()->siswa->kelas }}" name="kelas" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="jurusan">Jurusan</label>
                                <select class="form-control" name="jurusan" readonly id="jurusan" required>
                                    <option value="{{ Auth::user()->siswa->jurusan }}" readonly selected>{{ Auth::user()->siswa->jurusan }}</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        @if(Auth::user()->siswa->nis == Auth::user()->verif_id)
                        <p class="text text-success">Data telah diverifikasi !</p>
                        @else
                        <a class="btn btn-primary btn-sm" href="{{ route('siswa_edit', Auth::user()->siswa->nis) }}">Ubah</a>
                        <button class="btn btn-success btn-sm" type="submit">Verifikasi Data</button>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
