<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PPDB SMK Merdeka Bisa</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>
   <div class="container">
    <nav class="navbar navbar-expand-lg navbar-light p-0 mt-2">
        <a class="navbar-brand">SMK Merdeka Bisa</a>
        <button class="navbar-toggler" data-target="#my-nav" data-toggle="collapse" aria-controls="my-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div id="my-nav" class="collapse navbar-collapse">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="btn btn-primary" href="{{ route('login') }}">Login</a>
                </li>
            </ul>
        </div>
    </nav>

    <section class="content mt-5">
        <div class="row">
            <div class="col-md-6" style="margin-top: 100px;">
                <p class="text text-secondary m-0">Selamat Datang di</p>
                <h3 class="h3 font-weight-bold">PPDB SMK Merdeka Bisa</h3>
                <a class="btn btn-primary rounded-pill px-4" href="{{ route('siswa_daftar') }}">Daftar Sekarang</a>
            </div>
            <div class="col-md-6">
                <img class="img-fluid" src="{{ asset('sawah.png') }}" alt="sawah.png">
            </div>
        </div>
    </section>
</div>

    <footer class="fixed-bottom">
        <img src="{{ asset('wave.svg') }}" alt="wave.svg">
    </footer>

</body>
</html>
