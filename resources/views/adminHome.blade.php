@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if ($message = Session::get('error'))
                    <div class="alert alert-danger" role="alert">
                        {{ $message }}
                    </div>
                @endif

                @if ($errors->any())
                    <div class="alert alert-danger" role="alert">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if ($message = Session::get('success'))
                    <div class="alert alert-success" role="alert">
                        {{ $message }}
                    </div>
                @endif

                    <form action="{{ route('admin.home') }}" method="GET">
                        {{ csrf_field() }}
                        <div class="row px-3">
                            <input class="form-control col-md-4" type="text" name="search" value="{{ request()->query('search') }}" placeholder="Cari..">
                            <a class="btn btn-info btn-sm ml-auto col-2 pt-2" href="{{route('export_excel') }}">Export as Excel</a>
                        </div>
                    </form>
                    <br>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>NIS</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Jenis Kelamin</th>
                                <th>Tempat Lahir</th>
                                <th>Tanggal Lahir</th>
                                <th>Alamat</th>
                                <th>Asal Sekolah</th>
                                <th>Kelas</th>
                                <th>Jurusan</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($verifs as $verif)
                                <tr>
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $verif->nis }}</td>
                                    <td>{{ $verif->nama }}</td>
                                    <td>{{ $verif->email }}</td>
                                    <td>{{ $verif->jk }}</td>
                                    <td>{{ $verif->temp_lahir }}</td>
                                    <td>{{ \Carbon\Carbon::parse($verif->tgl_lahir)->IsoFormat('D MMM Y') }}</td>
                                    <td>{{ $verif->alamat }}</td>
                                    <td>{{ $verif->asal_sekolah }}</td>
                                    <td>{{ $verif->kelas }}</td>
                                    <td>{{ $verif->jurusan }}</td>
                                    <td>
                                        <form action="{{ route('siswa_delete', $verif->nis) }}" method="POST">
                                            @csrf
                                            @method('DELETE')


                                            <a class="btn btn-info btn-sm" href="{{ route('cetak_pdf', $verif->nis) }}">Print</a>
                                            <!-- Button trigger modal -->
                                            <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modelId">
                                              Hapus
                                            </button>

                                            <!-- Modal -->
                                            <div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            Yakin ingin dihapus ?
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button class="btn btn-danger" type="submit">Ya</button>
                                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Tidak</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="12" class="text-center">Data Kosong</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                    {!! $verifs->appends(['search' => request()->query('search')])->links() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
