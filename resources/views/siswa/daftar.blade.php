@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if ($message = Session::get('error'))
                        <div class="alert alert-danger" role="alert">
                            {{ $message }}
                        </div>
                    @endif

                    @if ($errors->any())
                        <div class="alert alert-danger" role="alert">
                           <ul>
                               @foreach ($errors->all() as $error)
                                   <li>{{ $error }}</li>
                               @endforeach
                           </ul>
                        </div>
                    @endif

                    <form action="{{ route('siswa_post') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="nis">NIS</label>
                                <input id="nis" class="form-control" type="text" name="nis" required>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="nama">Nama</label>
                                <input id="nama" class="form-control" type="text" name="nama" required>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="email">Email</label>
                                <input id="email" class="form-control" type="text" name="email" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="jk">Jenis Kelamin</label>
                                <select class="form-control" name="jk" id="jk" required>
                                    <option value="" selected disabled>Pilih Jenis Kelamin</option>
                                    <option value="Laki-Laki">Laki-Laki</option>
                                    <option value="Perempuan">Perempuan</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="temp_lahir">Tempat Lahir</label>
                                <input id="temp_lahir" class="form-control" type="text" name="temp_lahir" required>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="tgl_lahir">Tanggal Lahir</label>
                                <input id="tgl_lahir" class="form-control" type="date" name="tgl_lahir" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="alamat">Alamat</label>
                                <textarea class="form-control" name="alamat" id="alamat" cols="30" rows="10" required></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="asal_sekolah">Asal Sekolah</label>
                                <input id="asal_sekolah" class="form-control" type="text" name="asal_sekolah" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="kelas">Kelas</label>
                                <input id="kelas" class="form-control" type="text" name="kelas" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="jurusan">Jurusan</label>
                                <select class="form-control" name="jurusan" id="jurusan" required>
                                    <option value="" selected disabled>Pilih Jurusan</option>
                                    <option value="Rekayasa Perangkat Lunak">Rekayasa Perangkat Lunak</option>
                                    <option value="Multimedia">Multimedia</option>
                                    <option value="Teknik Komputer dan Jarigan">Teknik Komputer dan Jarigan</option>
                                    <option value="Bisnis Daring dan Pemasaran">Bisnis Daring dan Pemasaran</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <button class="btn btn-primary btn-sm" type="submit">Daftar</button>
                        <button class="btn btn-warning btn-sm" type="reset">Bersihkan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
