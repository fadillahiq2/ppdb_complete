<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data Siswa Print</title>
</head>
<body onload="window.print()">
        <h4>Informasi Data Siswa</h4>
        <table>
            <tr>
                <td>NIS : {{$siswa->nis}}</td>
            </tr>
            <tr>
                <td>Nama : {{ $siswa->nama }}</td>
            </tr>
            <tr>
                <td>Email : {{ $siswa->email }}</td>
            </tr>
            <tr>
                <td>Jenis Kelamin : {{ $siswa->jk }}</td>
            </tr>
            <tr>
                <td>Tempat Lahir : {{ $siswa->temp_lahir }}</td>
            </tr>
            <tr>
                <td>Tanggal Lahir : {{ \Carbon\Carbon::parse($siswa->tgl_lahir)->IsoFormat('D MMM Y') }}</td>
            </tr>
            <tr>
                <td>Alamat : {{ $siswa->alamat }}</td>
            </tr>
            <tr>
                <td>Asal Sekolah : {{ $siswa->asal_sekolah }}</td>
            </tr>
            <tr>
                <td>Kelas : {{ $siswa->kelas }}</td>
            </tr>
            <tr>
                <td>Jurusan : {{ $siswa->jurusan }}</td>
            </tr>
        </table>
        <br>
        <h4>Informasi Akses Login</h4>
        <p>Silahkan gunakan Email dan Password dibawah ini untuk akses login</p>
        <table>
            <tr>
                <td>Email : {{ $siswa->email }}</td>
            </tr>
            <tr>
                <td>Password : {{ $siswa->nis }}</td>
            </tr>
        </table>
        <p>Link Login : <a href="{{ route('login') }}">Klik Disini</a></p>
    </div>
</body>
</html>
