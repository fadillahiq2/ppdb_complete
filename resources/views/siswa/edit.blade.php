@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if ($message = Session::get('error'))
                        <div class="alert alert-danger" role="alert">
                            {{ $message }}
                        </div>
                    @endif

                    @if ($errors->any())
                        <div class="alert alert-danger" role="alert">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @if ($message = Session::get('success'))
                        <div class="alert alert-success" role="alert">
                            {{ $message }}
                        </div>
                    @endif
                    <form action="{{ route('siswa_update', $siswa->nis) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="nama">Nama</label>
                                <input id="nama" class="form-control" value="{{ $siswa->nama }}" type="text" name="nama" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="jk">Jenis Kelamin</label>
                                <select class="form-control" name="jk" id="jk" required>
                                    <option value="Laki-Laki" @if($siswa->jk == "Laki-Laki") selected='selected' @endif>Laki-Laki</option>
                                    <option value="Perempuan" @if($siswa->jk == "Perempuan") selected='selected' @endif>Perempuan</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="temp_lahir">Tempat Lahir</label>
                                <input id="temp_lahir" class="form-control" value="{{ $siswa->temp_lahir }}" type="text" name="temp_lahir" required>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="tgl_lahir">Tanggal Lahir</label>
                                <input id="tgl_lahir" class="form-control" type="date" value="{{ $siswa->tgl_lahir }}" name="tgl_lahir" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="alamat">Alamat</label>
                                <textarea class="form-control" name="alamat" id="alamat" cols="30" rows="10" required>{{ $siswa->alamat }}</textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="asal_sekolah">Asal Sekolah</label>
                                <input id="asal_sekolah" class="form-control" type="text" value="{{ $siswa->asal_sekolah }}" name="asal_sekolah" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="kelas">Kelas</label>
                                <input id="kelas" class="form-control" type="text" value="{{ $siswa->kelas }}" name="kelas" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="jurusan">Jurusan</label>
                                <select class="form-control" name="jurusan" id="jurusan" required>
                                    <option value="Rekayasa Perangkat Lunak" @if($siswa->jurusan == "Rekayasa Perangkat Lunak") selected='selected' @endif>Rekayasa Perangkat Lunak</option>
                                    <option value="Multimedia" @if($siswa->jurusan == "Multimedia") selected='selected' @endif>Multimedia</option>
                                    <option value="Teknik Komputer dan Jarigan" @if($siswa->jurusan == "Teknik Komputer dan Jarigan") selected='selected' @endif>Teknik Komputer dan Jarigan</option>
                                    <option value="Bisnis Daring dan Pemasaran" @if($siswa->jurusan == "Bisnis Daring dan Pemasaran") selected='selected' @endif>Bisnis Daring dan Pemasaran</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <button class="btn btn-warning btn-sm" type="submit">Perbarui</button>
                        <a class="btn btn-info btn-sm" href="{{ route('home') }}">Kembali</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
