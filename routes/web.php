<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\SiswaController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/siswa/daftar', [SiswaController::class, 'siswa_daftar'])->name('siswa_daftar');
Route::post('/siswa/daftarPost', [SiswaController::class, 'siswa_post'])->name('siswa_post');
Route::get('/siswa/print/{nis}', [SiswaController::class, 'siswa_print'])->name('siswa_print');

Auth::routes([
    'register' => false,
    'verify' => false
]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/home/edit/{nis}', [HomeController::class, 'siswa_edit'])->name('siswa_edit');
Route::put('/home/update/{nis}', [HomeController::class, 'siswa_update'])->name('siswa_update');
Route::post('/home/verif/{id}', [HomeController::class, 'siswa_verif'])->name('siswa_verif');

Route::get('/admin', [App\Http\Controllers\HomeController::class, 'admin'])->middleware('is_admin')->name('admin.home');
Route::delete('/admin/delete/{nis}', [App\Http\Controllers\HomeController::class, 'siswa_delete'])->middleware('is_admin')->name('siswa_delete');
Route::get('/excel/export', [HomeController::class, 'excel'])->middleware('is_admin')->name('export_excel');
Route::get('/pdf/{nis}', [HomeController::class, 'cetak_pdf'])->middleware('is_admin')->name('cetak_pdf');



