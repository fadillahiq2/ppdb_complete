<?php

namespace App\Http\Controllers;

use App\Models\Siswa;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class SiswaController extends Controller
{
    public function siswa_daftar()
    {
        return view('siswa.daftar');
    }

    public function siswa_print($nis)
    {
        $siswa = Siswa::find($nis);
        return view('siswa.print', compact('siswa'));
    }

    public function siswa_post(Request $request)
    {
        $check = DB::table('siswas')->where('nis', $request->nis)->first();

        $this->validate($request, [
            'nis' => 'required',
            'nama' => 'required',
            'email' => 'required|unique:siswas',
            'jk' => 'required',
            'temp_lahir' => 'required',
            'tgl_lahir' => 'required',
            'alamat' => 'required',
            'asal_sekolah' => 'required',
            'kelas' => 'required',
            'jurusan' => 'required',
        ]);

        if(!$check)
        {
            Siswa::create($request->all());

            $user = new User;
            $user->name = $request->nama;
            $user->email = $request->email;
            $user->password = bcrypt($request->nis);
            $user->remember_token = Str::random(60);
            $user->is_admin = 0;
            $user->siswa_id = $request->nis;
            $user->save();

            return redirect()->route('siswa_print', $request->nis);
        }else if($check)
        {
            return redirect()->route('siswa_daftar')->with('error', 'NIS Sudah Pernah Didaftarkan !!!');
        }


    }
}
