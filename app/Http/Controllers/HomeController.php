<?php

namespace App\Http\Controllers;

use App\Exports\VerifExport;
use App\Models\Siswa;
use App\Models\SiswaVerif;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function admin()
    {
        Paginator::useBootstrap();

        $search = request()->query('search');
        if($search)
        {
            $verifs = SiswaVerif::where('nis', 'LIKE', "%{$search}%")->OrWhere('nama', 'LIKE', "%{$search}%")->paginate(10);
        }else {
            $verifs = SiswaVerif::latest()->paginate(10);
        }

        return view('adminHome', compact('verifs'))->with('i');
    }

    public function siswa_edit($nis)
    {
        $siswa = Siswa::findOrFail($nis);

        return view('siswa.edit', compact('siswa'));
    }

    public function siswa_update(Request $request, $nis)
    {
        $this->validate($request, [
            'nama' => 'required',
            'jk' => 'required',
            'temp_lahir' => 'required',
            'tgl_lahir' => 'required',
            'alamat' => 'required',
            'asal_sekolah' => 'required',
            'kelas' => 'required',
            'jurusan' => 'required',
        ]);

        $siswa = Siswa::findOrFail($nis);
        $siswa->update($request->all());

        return redirect()->route('home')->with('success', 'Data siswa berhasil diperbaui !');
    }

    public function siswa_verif(Request $request, $id)
    {
        $this->validate($request, [
            'nis' => 'required',
            'nama' => 'required',
            'email' => 'required',
            'jk' => 'required',
            'temp_lahir' => 'required',
            'tgl_lahir' => 'required',
            'alamat' => 'required',
            'asal_sekolah' => 'required',
            'kelas' => 'required',
            'jurusan' => 'required',
        ]);

        SiswaVerif::create($request->all());

        $verif = User::findOrFail($id);
        $verif->update([
            'verif_id' => $request->nis
        ]);

        return redirect()->route('home')->with('success', 'Data berhasil diverifikasi !');
    }

    public function siswa_delete($nis)
    {
        $verif = SiswaVerif::findOrFail($nis);
        $verif->delete();

        return redirect()->route('admin.home')->with('success', 'Verifikasi berhasil dibatalkan');
    }


    public function excel()
    {
        return Excel::Download(new VerifExport, 'ppdb_siswa.xlsx');
    }

    public function cetak_pdf($nis)
    {
    $verif = SiswaVerif::findOrFail($nis);

    $pdf = PDF::Loadview('siswa.print', ['siswa' => $verif]);
    return $pdf->download('data-siswa.pdf');
    }
}
