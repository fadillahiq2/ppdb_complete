<?php

namespace App\Exports;

use App\SiswaVerif;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class VerifExport implements FromCollection, WithHeadings, ShouldAutoSize
{

    public function headings(): array
    {
        return [
            'NIS',
            'Nama',
            'Email',
            'Jenis Kelamin',
            'Tempat Lahir',
            'Tanggal Lahir',
            'Alamat',
            'Asal Sekolah',
            'Kelas',
            'Minat Jurusan'
        ];
    }


    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return DB::table('siswa_verifs')->select('nis', 'nama', 'email', 'jk', 'temp_lahir', 'tgl_lahir', 'alamat', 'asal_sekolah', 'kelas', 'jurusan')->get();
    }
}
